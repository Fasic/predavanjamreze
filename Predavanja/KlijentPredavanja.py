import socket
import pickle

HOST, PORT = "192.168.81.10", 6500
code = -1

podatak = input(">")

podatak = {'komanda': 'Z'}
podatakByte = pickle.dumps(podatak)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(podatakByte, (HOST, PORT))

primljniOdgovor = sock.recv(1024)
primljniOdgovorRecnik = pickle.loads(primljniOdgovor)
print(primljniOdgovorRecnik)

podatak = {'komanda': 'L',
           'ime': 'Filip',
           'prezime': 'Vasic',
           'br': 'None'}  # recnik
if('code' in primljniOdgovorRecnik):
    podatak['code'] = primljniOdgovorRecnik['code']
    code = primljniOdgovorRecnik['code']

podatakByte = pickle.dumps(podatak)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(podatakByte, (HOST, PORT))
primljniOdgovorRecnik = {"komanda": ""}

def sendData(sock, podatak):
    # if(code != -1):
    #     podatak['code'] = code
    podatakByte = pickle.dumps(podatak)
    sock.sendto(podatakByte, (HOST, PORT))

def printPrognoza(primljniOdgovorRecnik):
    print("Temp: " + str(primljniOdgovorRecnik["t"]))
    print("Vlaznost: " + str(primljniOdgovorRecnik["v"]) + " %")
    print("Sanse za kisom: " + str(primljniOdgovorRecnik["s"]) + " %")

def ispisMenija(primljniOdgovorRecnik):
    for i in primljniOdgovorRecnik:
        if (i != 'komanda'):
            print(i + ". " + primljniOdgovorRecnik[i])

while(primljniOdgovorRecnik["komanda"] != "E"):
    primljniOdgovor = sock.recv(1024)
    primljniOdgovorRecnik = pickle.loads(primljniOdgovor)

    if (primljniOdgovorRecnik["komanda"] == "M"):
        ispisMenija(primljniOdgovorRecnik)
        izbor = int(input(">"))
        podatak = {'komanda': 'I', 'izbor': izbor}
        if(izbor == 3):
            podatak['b1'] = int(input("B1>"))
            podatak['b2'] = int(input("B2>"))
            podatak['op'] = input("OP>") #dorada pomocu meni
        sendData(sock, podatak)
    elif(primljniOdgovorRecnik["komanda"] == "B"):
        print(primljniOdgovorRecnik["broj"])
        sendData(sock, {'komanda': 'G'})
    elif(primljniOdgovorRecnik["komanda"] == "P"):
        printPrognoza(primljniOdgovorRecnik)
        sendData(sock, {'komanda': 'G'})

print("Klijent zavrsio!")
