import socketserver
import pickle
import random

opcije = {'komanda': 'M', '1':'Nasumicni broj', '2':'Prognoza', '3':'Kalkulator', '0':'Exit'}

temp = random.randint(-20, 50)
vla = random.randint(0, 100)
sanse = random.randint(0, 100)

loginZahtevi = []
ulogovani = []

def sendData(socket, adresa, podatak):
    porukaZaOdgovorByte = pickle.dumps(podatak)
    socket.sendto(porukaZaOdgovorByte, adresa)

class MyUDPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        primljeniPodatak = self.request[0].strip()
        socket = self.request[1]
        y = pickle.loads(primljeniPodatak) #picklePrimljeniPodatak
        print(y)
        if(y["komanda"] == "Z"):
            code = random.randint(1000,9999)
            loginZahtevi.append(code)

            odgovor = {"komanda": "LZ", "code": code}
            sendData(socket, self.client_address, odgovor)

        elif(y["komanda"] == "L"):
            if("code" in y and y["code"] in loginZahtevi):
                loginZahtevi.remove(y["code"])
                ulogovani.append(y["code"])
                print("Login: " + y["ime"] + " " + y["prezime"] + " " + y["br"])
                sendData(socket, self.client_address, opcije)
            else:
                sendData(socket, self.client_address, {"komanda": "E"})
        elif(y["komanda"] == "G" and y["code"] in ulogovani):
            sendData(socket, self.client_address, opcije)
        elif(y["komanda"] == "I" and y["code"] in ulogovani):
            if(y["izbor"] == 0):
                sendData(socket, self.client_address, {"komanda": "E"})
            elif(y["izbor"] == 1):
                broj = random.randint(1,100)
                odgovor = {"komanda": "B", "broj": broj}
                sendData(socket, self.client_address, odgovor)
            elif (y["izbor"] == 2):
                odgovor = {"komanda": "P", "t": temp, "v": vla, "s": sanse}
                sendData(socket, self.client_address, odgovor)
            elif (y["izbor"] == 3):
                b1 = y["b1"]
                b2 = y["b2"]
                op = y["op"]
                if(op == "*"):
                    broj = b1 * b2
                elif (op == "+"):
                    broj = b1 + b2
                elif (op == "-"):
                    broj = b1 - b2
                elif (op == "/"):
                    broj = b1 / b2
                odgovor = {"komanda": "B", "broj": broj}
                sendData(socket, self.client_address, odgovor)

HOST = "192.168.81.10"
PORT = 6500
server = socketserver.UDPServer((HOST, PORT), MyUDPHandler)
server.serve_forever()
