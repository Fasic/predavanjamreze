import socket
import pickle

#{'ime': 'Filip', 'prezime': 'Vasic', 'brIndeksa': '123'}
HOST, PORT = "192.168.81.10", 6500

podatak = ""
while(podatak != "Exit"):
    podatak = input(">")

    podatak = {'komanda': 'GET'}  # recnik
    podatakByte = pickle.dumps(podatak)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(podatakByte, (HOST, PORT))

    primljniOdgovor = sock.recv(1024)
    primljniOdgovorString = primljniOdgovor.decode("utf-8")
    dal = False
    if(primljniOdgovorString == "1"):
        podatak = {'ime': 'Filip'}  # recnik
        dal = True
    if (primljniOdgovorString == "2"):
        podatak = {'prezime': 'Vasic'}  # recnik
        dal = True
    if (primljniOdgovorString == "3"):
        podatak = {'ime': '123'}  # recnik
        dal = True
    if(primljniOdgovorString == "Ok"):
        print("Gotov zadatak")

    if(dal):
        podatakByte = pickle.dumps(podatak)

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(podatakByte, (HOST, PORT))