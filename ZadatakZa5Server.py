import socketserver
import pickle
import random

komanda = 'komanda'
moguciParametriP = ['ime', 'prezime', 'indeks']
opcijeMenija = {komanda: "M", 1:'kalkulator', 2:'zasticeno', 3:'izlaz'}
racunskeOperacije = ["+","-","*","/"]
users = {}

class MyUDPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        primljeniPodatak = self.request[0].strip()
        socket = self.request[1]
        primljenoRecnik = pickle.loads(primljeniPodatak)

        if(komanda in primljenoRecnik):
            k = primljenoRecnik[komanda].upper()
            if(k == 'L'):
                self.send(self.getParametri(), socket, self.client_address)
            elif(primljenoRecnik[komanda] == 'PL'):
                print("Login by:", primljenoRecnik['1'], primljenoRecnik['2'])
                self.send(opcijeMenija, socket, self.client_address)
            elif (primljenoRecnik[komanda] == 'I'):
                if('broj' in primljenoRecnik):
                    izbor = primljenoRecnik['broj']
                    if(izbor == 1):
                        self.send(self.getPitanje(), socket, self.client_address)
                    elif(izbor == 2):
                        self.send(self.getAcessKey(), socket, self.client_address)
                    elif(izbor == 3):
                        self.send({komanda: 'E'}, socket, self.client_address)
            elif(primljenoRecnik[komanda] == 'A'):
                print("Odgovor na postavljeno pitanje je:", primljenoRecnik[1])
                if(random.randint(1,50) %2 == 0):
                    self.send(self.getPitanje(), socket, self.client_address)
                else:
                    self.send({komanda: 'E'}, socket, self.client_address)
            elif(primljenoRecnik[komanda] == 'LS'):
                print(primljenoRecnik)
                self.send({komanda: 'E'}, socket, self.client_address)

    def send(self, podatak, socket, adresa):
        porukaZaOdgovorByte = pickle.dumps(podatak)
        socket.sendto(porukaZaOdgovorByte, adresa)

    def getParametri(self):
        random.shuffle(moguciParametriP)
        return {
            komanda: 'P',
            1: moguciParametriP[0],
            2: moguciParametriP[1],
        }

    def getPitanje(self):
        return {
            komanda: 'Q',
            1: racunskeOperacije[random.randint(0,3)],
            2: random.randint(-5000,5000),
            3: random.randint(-5000, 5000)
        }

    def getAcessKey(self):
        return {
            komanda: 'S',
            'akey': random.randint(1000,9999)
        }


class MyTCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request.recv(1024)
        print("Poruka od {} :".format(self.client_address[0]))

        data = data.decode("utf-8")
        print(data)

        self.request.sendall(data.upper().encode())

HOST = "192.168.81.10"
PORT = 1212
server = socketserver.UDPServer((HOST, PORT), MyUDPHandler)
server.serve_forever()